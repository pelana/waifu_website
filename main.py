# encoding=utf-8
from importd import d
from django.conf.urls import include
from django.utils.translation import ugettext_lazy as _

d(DEBUG=True, USE_I18N=True, LANGUAGES=(
        ('es', _(u'Español')), 
        ('en', _('English')), 
        ('pt', _('Portugues')), 
        ('fr',_("French"))),
        MIDDLEWARE_CLASSES= [
       'django.contrib.sessions.middleware.SessionMiddleware',
               'django.middleware.locale.LocaleMiddleware',
             'django.middleware.common.CommonMiddleware',
              'django.middleware.csrf.CsrfViewMiddleware'],
              LOCALE_PATHS=["locale",])
d(d.patterns("",
    (r'^i18n/', include('django.conf.urls.i18n')),

))
d(INSTALLED_APPS=["color_captcha"])

@d
def terms(request):
    return "terms.html"
@d
def contact(request):
	return 'contact.html'
	
@d 
def uninstall(request):
    return "uninstall.html"
@d
def uninstall(request):
	return 'uninstall.html'
	
@d("/sitemap.xml")
def index(request):
    return "sitemap.xml"
@d("/robots.txt")
def robo(request):
    return "robots.txt"
def win(request):
    return "win.html"

from color_captcha.fields import ColorCaptchaField


class V(d.forms.Form):
    captcha = ColorCaptchaField(label="Choose a color")

@d("/")
def index(request):
    f = V()
    if request.method=="POST":
        f = V(request.POST)
        if f.is_valid():
            res = d.HttpResponse()
            del res['content-type'] 
            res["X-Accel-Redirect"]="/dd/setup_waifu.exe"
            res["Content-Disposition"] = "attachment; filename=setup_waifu.exe"
            return res
    return "index.html", {'form':f}

application = d.wsgi_application

if __name__ == "__main__":
    d.main()
